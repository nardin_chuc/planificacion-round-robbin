/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Hilos;

import java.awt.Color;
import static java.awt.Color.RED;
import java.awt.Font;
import java.awt.Graphics;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DibujarRectangulo {
    Color color;
    Color Rectangulo;
    
    
    public DibujarRectangulo (Graphics g, int x , int y, String num,Color color){
        
        for (int i = 0; i <=40;i++) {              //Medida del rectangulo
            g.clearRect(x, y, i, i);
            g.setColor(Color.BLACK);
            g.drawRect(x, y, i, i);
            g.setColor(color);
            g.fillRect(x, y, i, i);
            g.setColor(Color.WHITE);
            
            if (i>=30) {
                g.setFont(new Font ("Arial" , Font.BOLD,i-10));
                g.drawString(num, x+6 , y + 27);  
            }


            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(DibujarRectangulo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
