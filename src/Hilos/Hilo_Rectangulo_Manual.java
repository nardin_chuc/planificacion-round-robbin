/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Hilos;

import RR_Vista.InterfazRR;
import RR_Vista.ManualRR;
import java.awt.Color;
import static java.awt.Color.BLUE;
import static java.awt.Color.CYAN;
import static java.awt.Color.GREEN;
import static java.awt.Color.magenta;
import static java.awt.Color.orange;
import java.awt.Graphics;
import java.lang.reflect.Array;

public class Hilo_Rectangulo_Manual extends Thread {

    String valor = "";
    int nume, aux, contador = 0, conta = 0, p1 = 0, p2 = 0, p3 = 0, p4 = 0, p5 = 0, lugar = 0;
    int contaP1 = 0, contaP2 = 0, contaP3 = 0, contaP4 = 0,contaP5= 0;
    public int NumeroMayor = 0;
    int[] Timeproceso = new int[5];
    int[] guia = new int[5];
    int x = 90;
    int y;
    DibujarRectangulo r;
    private HiloTiempo_Manual hT;

    private ManualRR RRM;
    
     public Hilo_Rectangulo_Manual(ManualRR RRM) {
        this.RRM = RRM;

    }

    @Override
    public void run() {

        if (contador == 4) {

        } else {
            for (int i = 0; i < 5; i++) {
                meterNumero(contador);
                contador++;
                guia[i] = Timeproceso[i];
                NumeroMayor += Timeproceso[i];

            }

           RRM.jLabel_p1.setText("" + Timeproceso[0] + "s.");
            RRM.jLabel_p2.setText("" + Timeproceso[1] + "s.");
            RRM.jLabel_p3.setText("" + Timeproceso[2] + "s.");
            RRM.jLabel_p4.setText("" + Timeproceso[3] + "s.");
            RRM.jLabel_p5.setText("" + Timeproceso[4] + "s.");
            try {
                Thread.sleep(1200);
            } catch (Exception e) {

            }
            hT = new HiloTiempo_Manual(RRM);
            hT.setLimite(NumeroMayor);
            hT.start();
            ordenar();
            RRM.btn_Iniciar.setVisible(true);
        }

    }

    public void buscarNumero(int numero) {
        for (int i = 0; i < contador; i++) {
            if (Timeproceso[i] == numero) {                                                    //Validacion si se encuentra en el arreglo
                meterNumero(contador);                                                                    //llama al metodo de meter numero para agregar otro
            } else {

            }
        }
    }

    public void meterNumero(int contador) {   //Metodo Principal para meter numero al arreglo numeros
        aux = (int) Math.floor(Math.random() * 9);                                                     //aux recibira el numero aleatorio
        if (aux == 0) {                                                                                  //Valida si es igual a cero, ya que en las cartas no carta 0
            aux = aux + 1;                                                                               //Si hay un cero, se le sumara un uno, para evitarlo       
        }
        for (int i = contador; i < contador + 1; i++) {
            Timeproceso[i] = aux;
            buscarNumero(aux);

        }
    }

    public int getNumeroMayor() {
        return NumeroMayor;
    }

    public void setNumeroMayor(int NumeroMayor) {
        this.NumeroMayor = NumeroMayor;
    }

    public void dibujar(int limite, int altura, Color color) {
        y = altura;
        for (int i = 0; i < limite; i++) {
            valor = String.valueOf(limite);
        }
        r = new DibujarRectangulo(RRM.JpanelAnimar.getGraphics(), x, y, valor, color);

        x = x + 45;
    }

    public void ordenar() {
        MergeSort MS =  new MergeSort(Timeproceso);
        MS.sort(Timeproceso, 0, (Timeproceso.length - 1));
        int UltimoNumMayor = Timeproceso[4];
        int quantum = Integer.parseInt(RRM.Txt_Quantum.getText());
        

        for (int p = 0; p < UltimoNumMayor; p++) {
            if (contaP1 <= guia[0]) {
                p1 = guia[0];
                double num1 = p1 - contaP1;
                if (num1 < 1 || num1 <= quantum) {
                    contaP1 += num1 + quantum;
                    for (int j = 0; j < num1; j++) {
                        dibujar(p1, 10, GREEN);
                    }
                } else if (num1 <= guia[0]) {
                    for (int j = 0; j < quantum; j++) {
                        dibujar(p1, 10, GREEN);
                    }
                    contaP1 += quantum;
                }

            }
            if (contaP2 <= guia[1]) {
                p2 = guia[1];
                double num2 = p2 - contaP2;
                if (num2 < 1 || num2 <= quantum) {
                    contaP2 += num2 + quantum;
                    for (int k = 0; k < num2; k++) {
                        dibujar(p2, 60, orange);
                    }
                } else if (num2 <= guia[1]) {
                    for (int k = 0; k < quantum; k++) {
                        dibujar(p2, 60,orange);
                    }
                    contaP2 += quantum;
                }
            }
            if(contaP3 <= guia[2]){
                p3 = guia[2];
                double num3 = p3 - contaP3;
                if (num3 < 1 || num3 <= quantum) {
                    contaP3 += num3 + quantum;
                    for (int k = 0; k < num3; k++) {
                        dibujar(p3, 110, magenta);
                    }
                } else if (num3 > quantum) {
                    for (int k = 0; k < quantum; k++) {
                        dibujar(p3, 110, magenta);
                    }
                    contaP3 += quantum;
                }
            }
            if(contaP4 <= guia[3]){
                p4 = guia[3];
                double num4 = p4 - contaP4;
                if (num4 < 1 || num4 <= quantum) {
                    contaP4 += num4 + quantum;
                    for (int k = 0; k < num4; k++) {
                        dibujar(p4, 160, BLUE);
                    }
                } else if (num4 > quantum) {
                    for (int k = 0; k < quantum; k++) {
                        dibujar(p4, 160, BLUE);
                    }
                    contaP4 += quantum;
                }
            }
            if(contaP5 <= guia[4]){
                p5 = guia[4];
                double num5 = p5 - contaP5;
                if (num5< 1 || num5 <= quantum) {
                    contaP5 += num5 + quantum;
                    for (int k = 0; k < num5; k++) {
                        dibujar(p5, 210, CYAN);
                    }
                } else if (num5 > quantum) {
                    for (int k = 0; k < quantum; k++) {
                        dibujar(p5, 210, CYAN);
                    }
                    contaP5 += quantum;
                }
        }
    }
}
}
