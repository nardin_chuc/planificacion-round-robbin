/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Hilos;

import RR_Vista.InterfazRR;
import RR_Vista.ManualRR;
import java.awt.Color;
import static java.awt.Color.BLACK;

public class HiloTiempo_Manual extends Thread {
 
    private ManualRR RRM;
    DibujarRectangulo r;
    Hilo_Rectangulo rec;
    String valor = "";
    int limite = 0 ;
    int x = 90;
    int y = 10;

    
    public void setLimite(int limite) {
        this.limite = limite;
    }
    
    
    public HiloTiempo_Manual(ManualRR RRM){
        this.RRM = RRM;
    }
    
    @Override
    public void run(){
        
        for (int i = 0; i < limite; i++) {
                dibujar(i);
            }
        
    }
    
    public void dibujar(int tiempo) {
      
        for (int i = 0; i < 20; i++) {
            valor = String.valueOf(tiempo);
        }
        r = new DibujarRectangulo(RRM.jPanel_Tiempo.getGraphics(), x, y, valor, BLACK);

        x = x + 45;
    }
}
