/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Hilos;

import RR_Vista.InterfazRR;
import java.awt.Color;
import static java.awt.Color.BLACK;

public class HiloTiempo extends Thread {
 
    private InterfazRR RR;
    DibujarRectangulo r;
    Hilo_Rectangulo rec;
    String valor = "";
    int limite = 0 ;
    int x = 90;
    int y = 10;

    
    public void setLimite(int limite) {
        this.limite = limite;
    }
    
    
    public HiloTiempo(InterfazRR RR){
        this.RR = RR;
    }
    
    @Override
    public void run(){
        
        for (int i = 0; i < limite; i++) {
                dibujar(i);
            }
        
    }
    
    public void dibujar(int tiempo) {
      
        for (int i = 0; i < 20; i++) {
            valor = String.valueOf(tiempo);
        }
        r = new DibujarRectangulo(RR.jPanel_Tiempo.getGraphics(), x, y, valor, BLACK);

        x = x + 45;
    }
}
