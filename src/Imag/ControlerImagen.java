/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Imag;

import RR_Vista.Portada;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author CCNAR
 */
public class ControlerImagen {
    
    
    private Portada pt;
    
    public ControlerImagen(Portada pt){
        this.pt = pt;
        cambiarImagen();
    }
    
    
    public void cambiarImagen(){
        ImageIcon wallpaper = new ImageIcon("src/Imag/Foto_uac.jpg");//Busca la imagen el paquete Imagen con el nombre propuesto
                         
            Icon icono = new ImageIcon(wallpaper.getImage().getScaledInstance(pt.jLabel1.getWidth(),pt.jLabel1.getHeight(), Image.SCALE_DEFAULT));
            pt.jLabel1.setIcon(icono);            
}
}
