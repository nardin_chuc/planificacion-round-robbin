/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Hilos.HiloTiempo;
import Hilos.Hilo_Rectangulo;
import RR_Vista.InterfazRR;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;


public class ControladorInterfazRR implements ActionListener {
    
    private InterfazRR RR;
    private Hilo_Rectangulo hRec;
    int []arreglo = new int [4];
    

    public ControladorInterfazRR(InterfazRR RR){
        this.RR = RR;
        this.RR.btn_Iniciar.addActionListener(this);
        Ver(false);
        hRec = new Hilo_Rectangulo(RR);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == RR.btn_Iniciar){
           
            RR.btn_Iniciar.setVisible(false);
            RR.JpanelAnimar.setVisible(true);
            RR.jPanel_Tiempo.setVisible(true);
            Ver(true);
            Hilo_Rectangulo r1 = new Hilo_Rectangulo(RR);
            r1.start();
            
            RR.JpanelAnimar.repaint();
            RR.jPanel_Tiempo.repaint();
        }
            
    }
    public void Ver(boolean ver){
        RR.jPanel_Datos.setVisible(ver);
    }
    
}
