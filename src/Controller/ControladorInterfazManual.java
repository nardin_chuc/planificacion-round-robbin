/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Hilos.*;
import RR_Vista.ManualRR;
import java.awt.Color;
import java.awt.event.*;
import java.util.HashSet;
import javax.swing.JOptionPane;


public class ControladorInterfazManual implements ActionListener, MouseListener {
    
    private ManualRR RRM;
    private Hilo_Rectangulo_Manual hRec;
    private Color color = new Color (214,217,223);
    

    public ControladorInterfazManual(ManualRR RRM){
        this.RRM = RRM;
        this.RRM.btn_Iniciar.addActionListener(this);
        this.RRM.btn_Iniciar.addMouseListener(this);
        this.RRM.Txt_Quantum.addActionListener(this);
        Ver(false);
        hRec = new Hilo_Rectangulo_Manual(RRM);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == RRM.btn_Iniciar){
           if(RRM.Txt_Quantum.getText().isEmpty() || RRM.Txt_Quantum.getText().equals("0")){
               JOptionPane.showMessageDialog(null, "Dijite el Valor de Quantum\n"+"No debe ir vacio","Campos Vacios",JOptionPane.ERROR_MESSAGE);
               RRM.Txt_Quantum.setText("");
               RRM.Txt_Quantum.isFocusable();
           }else{
            RRM.btn_Iniciar.setVisible(false);
            RRM.JpanelAnimar.setVisible(true);
            RRM.jPanel_Tiempo.setVisible(true);
            Ver(true);
            Hilo_Rectangulo_Manual r1 = new Hilo_Rectangulo_Manual(RRM);
            r1.start();
            RRM.JpanelAnimar.repaint();
            RRM.jPanel_Tiempo.repaint(); 
            
           }
            
        }
            
    }
    public void Ver(boolean ver){
        RRM.jPanel_Datos.setVisible(ver);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
       
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
   
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    if(e.getSource() == RRM.btn_Iniciar){
        RRM.btn_Iniciar.setBackground(Color.red);
     }
    }

    @Override
    public void mouseExited(MouseEvent e) {
    if(e.getSource() == RRM.btn_Iniciar){
        RRM.btn_Iniciar.setBackground(color);
     }
    }
    
}
