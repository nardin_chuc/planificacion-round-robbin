/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import RR_Vista.*;
import java.awt.Color;
import java.awt.event.*;

/**
 *
 * @author CCNAR
 */
public class ControladorPortada implements ActionListener, MouseListener {
    
    
    private Portada pt;
    private Color color = new Color (214,217,223);
    
    public ControladorPortada(Portada pt){
        this.pt=pt;
        this.pt.btn_Auto.addActionListener(this);
        this.pt.btn_Manual.addActionListener(this);
        this.pt.btn_Auto.addMouseListener(this);
        this.pt.btn_Manual.addMouseListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if(e.getSource() == pt.btn_Manual){
          ManualRR MR = new ManualRR();
          MR.setVisible(true);
      }else if(e.getSource() == pt.btn_Auto){
          InterfazRR rr = new InterfazRR();
          rr.setVisible(true);
      }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
       
    }

    @Override
    public void mousePressed(MouseEvent e) {
       
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {  
    if(e.getSource() == pt.btn_Auto){
        pt.btn_Auto.setBackground(Color.red);
     }else if(e.getSource() == pt.btn_Manual){
        pt.btn_Manual.setBackground(Color.red);
     }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if(e.getSource() == pt.btn_Auto){
        pt.btn_Auto.setBackground(color);
     }else if(e.getSource() == pt.btn_Manual){
        pt.btn_Manual.setBackground(color);
     }
    }
    
}
